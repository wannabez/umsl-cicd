Due to the limited resources, I've rented small server in DigitalOcean, where I've set up following components:

1) Jenkins
2) SonarQube
3) UMSL application

The main code of the application is stored in my personal BitBucket repository:
https://bitbucket.org/wannabez/umsl/

CI/CD scripts are stored separately in another repository:
https://bitbucket.org/wannabez/umsl-cicd/
