node {
   stage('Preparation') {
      git branch: 'master',
        credentialsId: '2ae93bab-0a5f-42ca-8150-4925636a13a5',
        url: "git@bitbucket.org:wannabez/umsl.git"
        }
   stage('Build') {
      sh "./mvnw -Pprod clean"
   }
   stage('Packaging war file') {
       sh "./mvnw -Pprod,war clean"
   }
   stage('Tests') {
      sh "./mvnw verify"
   }
   stage('SQ Tests') {
      sh "./mvnw -Pprod clean verify sonar:sonar"
   }
   stage('Build Docker image'){
        sh "./mvnw -Pprod verify jib:dockerBuild"
   }
   stage('Push artefacts'){
     sh "docker login -uwannabez -pP@ssw0rd"
     sh "docker tag umsl:latest wannabez/umsl:latest"
     sh "docker push wannabez/umsl:latest"
   }
   stage('Deployment'){
     sh "ls -lah"
     sh "scp umsl-app.yml root@167.99.13.111:/root/umsl-app.yml"
     sh "ssh root@167.99.13.111 docker-compose -f /root/umsl-app.yml up -d"
   }
}
